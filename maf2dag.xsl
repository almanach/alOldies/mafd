<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:string="http://exslt.org/string"
  >

<xsl:output method="xml"
	    indent="yes"
	    omit-xml-declaration="yes"
	    encoding="ISO-8859-1"/>

<xsl:strip-space elements="*"/>

<xsl:variable name="root" select="."/>

<xsl:template match="maf">
 <xsl:text>
digraph G {
  graph [
    ranksep = 0.2,   
    rankdir = LR,
    ratio = auto
  ];
  node [style=filled,fillcolor="lightsalmon1"];
  </xsl:text>
  <xsl:apply-templates select="//wordForm"/>
  <xsl:text>
}
  </xsl:text>
</xsl:template>

<xsl:template match="transition/wordForm">
  <xsl:text>"</xsl:text>
  <xsl:value-of select="../@source"/>
  <xsl:text>"</xsl:text>
  <xsl:text disable-output-escaping="yes">-&gt;"</xsl:text>
  <xsl:value-of select="../@target"/>
  <xsl:text>" [ label=" </xsl:text>
<xsl:apply-templates select="." mode="label">
  <xsl:with-param name="source" select="../@source"/>
</xsl:apply-templates>
<xsl:text> ",]
</xsl:text>
</xsl:template>

<xsl:template match="transition/wfAlt/wordForm">
  <xsl:text>"</xsl:text>
  <xsl:value-of select="../../@source"/>
  <xsl:text>"</xsl:text>
  <xsl:text disable-output-escaping="yes">-&gt;"</xsl:text>
  <xsl:value-of select="../../@target"/>
  <xsl:text>" [ label=" </xsl:text>
<xsl:apply-templates select="." mode="label">
  <xsl:with-param name="source" select="../../@source"/>
</xsl:apply-templates>
<xsl:text> ",]
</xsl:text>
</xsl:template>

<xsl:template match="wordForm" mode="label">
  <xsl:param name="source"/>
  <xsl:choose>
    <xsl:when test="//transition[@target=$source]/wordForm/@tokens = ./@tokens">
      <xsl:text>*</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="label">
	<xsl:with-param name="tokens" select="./@tokens"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:text> \n</xsl:text>
  <!-- <xsl:value-of select="substring-before(substring-after(concat(@tag,' '),'pos@'),' ')"/> -->
  <xsl:value-of select="substring-after(substring-after(@entry,':'),':')"/>
  <xsl:text>_</xsl:text>
  <xsl:value-of select="fs/f[@name='pos']/symbol/@value"/>
  <xsl:text> {</xsl:text>
  <xsl:value-of select="./@tokens"/>
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template name="label">
  <xsl:param name="tokens"/>
  <xsl:choose>
    <xsl:when test="contains($tokens,' ')">
      <xsl:variable name="token" select="substring-before($tokens,' ')"/>
      <xsl:value-of select="//token[@id = $tokens]"/>
      <xsl:apply-templates select="//token[@id = $token]" mode="label"/>
      <xsl:text> </xsl:text>
      <xsl:call-template name="label">
        <xsl:with-param name="tokens" select="substring-after($tokens,' ')"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates select="//token[@id = $tokens]" mode="lastlabel"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="token" mode="label">
  <xsl:value-of select="."/>
</xsl:template>

<xsl:template match="token" mode="lastlabel">
  <xsl:value-of select="."/>
</xsl:template>

<xsl:template match="token[@value]" mode="lastlabel">
  <xsl:value-of select="."/>
  <xsl:text>/</xsl:text>
  <xsl:value-of select="@value"/>
</xsl:template>

</xsl:stylesheet>
