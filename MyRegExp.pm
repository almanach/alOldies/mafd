package MyRegExp;

use warnings;
use strict;

use vars qw($VERSION $RE @EXPORT);
@EXPORT = qw/$RE/;

$VERSION = '1.0';

$RE = {};

sub add_pattern {
  my ($key,$pat) = @_;
  $RE->{$key} = $pat;
}

######################################################################
## NUMBERS

add_pattern
  int_number => qr{ (?: \d+ (?: (?:\s+|[,.]) \d+)* ) }oix;

add_pattern 
  unite => qr{(?:deux|trois|quatre|cinq|six|sept|huit|neuf)}oix;

add_pattern 
  first => qr{(?:un(?:e)?)}oix;

add_pattern 
  xunite=> qr{(?: $RE->{first} | $RE->{unite} )}ox;

add_pattern 
  dizaine => qr{(?: (?:dix(?:-(sept|huit|neuf))?)
		| onze
		| douze
		| treize
		| quatorze
		| quinze
		| seize
	       )
	      }oix;

add_pattern 
  xdizaine => qr{(?: (?:(?:quatre-)?vingt)
		 | trente
		 | quarante
		 | cinquante
		 | soixante
		 | septante
		 | octante
		 | huitante
		 | nonante
		)
	       }oix;

add_pattern 
  ydizaine => qr{(?:soixante|quatre-vingt)}oix;

add_pattern 
  dizaines => qr{(?: $RE->{dizaine}
		 |  (?:$RE->{xdizaine} (?: \s+et\s+$RE->{first} | -$RE->{unite})? )
		 |  $RE->{ydizaine}-$RE->{dizaine}
		)
	       }oix;

add_pattern
  centaines => qr{ (?: (?: $RE->{int_number} | $RE->{unite}\s+)? cent  (?: \s+ (?:$RE->{xunite}|$RE->{dizaine}))? )
                   | (?: $RE->{unite} \s+ cents )
		}oix;

add_pattern
  milliers => qr{ (?: (?: $RE->{int_number} | $RE->{centaines} |  $RE->{dizaines} | $RE->{unite} ) \s+ )?
                 mil(?:le(?:s)?)?
                 (?: \s+ (?: $RE->{centaines} |  $RE->{dizaines} | $RE->{xunite} ) )?
	      }oix;

add_pattern
  millions => qr{ (?: (?: $RE->{int_number} | $RE->{centaines} |  $RE->{dizaines} | $RE->{xunite} ) \s+ )?
                 million
                 (?: \s+ (?: $RE->{milliers} | $RE->{centaines} |  $RE->{dizaines} | $RE->{xunite} ) )?
	       }oix;

add_pattern
  milliards => qr{ (?: (?: $RE->{int_number} | $RE->{centaines} |  $RE->{dizaines} | $RE->{xunite} ) \s+ )?
                 milliard(?:s)?
                 (?: \s+ (?: $RE->{millions} | $RE->{milliers} | $RE->{centaines} |  $RE->{dizaines} | $RE->{xunite} ) )?
	      }oix;

add_pattern 
  number => qr{(?: 
	       $RE->{milliards} 
	       | $RE->{millions}
	       | $RE->{milliers}
	       | $RE->{centaines}
	       | $RE->{dizaines}
	       | $RE->{unite}
	       | $RE->{int_number}
	      )
	     }oix;


######################################################################
## Ordinals

add_pattern
  term_ord => qr{(?:i�me(?:s)?)}oix;

add_pattern 
  unite_ord => qr{(?:deux|trois|quatr|cinqu|six|sept|huit|neuv)}oix;

add_pattern
  first_ord => qr{un}oix;


add_pattern 
  xunite_ord => qr{(?: $RE->{first_ord} | $RE->{unite_ord} )}ox;


add_pattern 
  dizaine_ord => qr{(?: (?:dix(?:-(sept|huit|neuv))?)
		    | onz
		    | douz
		    | treiz
		    | quatorz
		    | quinz
		    | seiz
		   )
		  }oix;

add_pattern 
  xdizaine_ord => qr{(?: (?:(?:quatre-)?vingt)
		     | trent
		     | quarant
		     | cinquant
		     | soixant
		     | septant
		     | octant
		     | huitant
		     | nonant
		    )
		   }oix;

add_pattern 
  dizaines_ord => qr{(?: $RE->{dizaine_ord}
		     |  (?:$RE->{xdizaine} (?: \s+et\s+$RE->{first_ord} | -$RE->{unite_ord})? )
		     |  $RE->{ydizaine}-$RE->{dizaine_ord}
		    )
	       }oix;

add_pattern
  centaines_ord => qr{ (?:$RE->{unite}\s+)? cent  (?: \s+ (?:$RE->{xunite_ord} | $RE->{dizaine_ord}))? }oix;

add_pattern
  milliers_ord => qr{ (?: (?: $RE->{centaines} |  $RE->{dizaines} | $RE->{unite} ) \s+ )? 
		      (?: mille \s+ (?: $RE->{centaines_ord} |  $RE->{dizaines_ord} | $RE->{xunite_ord} )
		      | mill
		    )
		   }oix;

add_pattern
  millions_ord => qr{ (?: (?: $RE->{centaines} |  $RE->{dizaines} | $RE->{xunite} ) \s+ )?
		      (?: million
		          \s+ (?: $RE->{milliers_ord} | $RE->{centaines_ord} |  $RE->{dizaines_ord} | $RE->{xunite_ord} )
		      | millionn
		      )
		   }oix;

add_pattern
  milliards_ord => qr{ (?: (?: $RE->{centaines} |  $RE->{dizaines} | $RE->{xunite} ) \s+ )?
		       milliard
		       (?: \s+ (?: $RE->{millions_ord} | $RE->{milliers_ord} | $RE->{centaines_ord} |  $RE->{dizaines_ord} | $RE->{xunite_ord} ) )?
	      }oix;

add_pattern 
  ordinal => qr{(?: premier(?:s)?
		| premi�re(?:s)?
		| second(?:e)?(?:s)?
		| (?:  $RE->{milliards_ord} 
		| $RE->{millions_ord}
		       | $RE->{milliers_ord}
		| $RE->{centaines_ord}
		| $RE->{dizaines_ord}
		| $RE->{unite_ord}
	       )
		$RE->{term_ord}
	       )
	      }oix;

######################################################################
## Dates

add_pattern
  mois => qr{ (?: janvier | janv\.?
	     | f�vrier | fevr?\.?
	     | mars?
	     | avril | avr\.?
	     | mai
	     | jui?n
	     | juillet | jui?l\.?
	     | ao�t
	     | septembre | sept\.?
	     | octobre | oct\.?
	     | novembre | nov\.?
	     | d�cembre | d�c\.?
	   )
	  }oix;

add_pattern
  jour => qr{ (?: lundi | mardi | mercredi | jeudi | vendredi | samedi | dimanche ) }oix;

add_pattern
  m_ord => qr{ (?: 1er | [012]\d | 3[01] | premier | $RE->{number} ) }oix;

add_pattern
  date => qr{ (?:
	      (?:  (?: $RE->{jour}\s+)? $RE->{m_ord} \s+)? $RE->{mois} (?: \s+ (?:[12]\d)?\d\d )?
	      | $RE->{jour} \s+ $RE->{m_ord} 
	      (?: \s+ de \s+ ce \s+ mois | \s+ de:prep \s+ le/det/ \s+ mois \s+ (?: prochain | dernier ))?
	    )
	   }oix;

1;
