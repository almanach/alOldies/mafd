/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2001, 2004 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  preheader.tag -- Header file for the Small English TAG Grammar
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-require 'tag_generic.pl'.

:-features( tag_tree, [ family, name, tree ] ).
:-features( tag_anchor, [name, coanchors, equations ] ).

:-tag_mode(_,all,[+|-],+,-).

:-features( lexicon, [lex,cat,ht,top] ).

:-extensional lexicon{}.


%% Translated using tag_converter
%% Input files frgram.tag.xml
%% Date Sun Nov 14 01:02:15 2004


:-require 'tag_generic.pl'.

%%% Features attached to each category

:-features('CS',[gender,mode,number,person,que,tense,wh]).
:-features('Infl',[aux_req,diathesis,gender,inv,mode,number,person,tense,wh]).
:-features('N',[gender,hum,number,person,time,wh]).
:-features('N2',[gender,hum,number,person,time,wh]).
:-features('PP',[extracted,gender,kind,number,pcas,person,real,wh]).
:-features('S',[extraction,gender,inv,mode,neg,number,person,sat,tense,wh,xarg]).
:-features('V',['Vadj',diathesis,gender,inv,mode,number,person,tense]).
:-features('V1',[gender,inv,mode,neg,number,person,tense]).
:-features('VArg',[gender,number,person]).
:-features(adj,[gender,number,person]).
:-features(adjP,[gender,number,person]).
:-features(advneg,[neg]).
:-features(arg,[extracted,kind,pcas,real]).
:-features(aux,[aspect,aux_req,diathesis,form_aux,gender,mode,number,person,tense]).
:-features(cla,[case,extracted,gender,kind,number,pcas,person,real]).
:-features(cld,[case,extracted,gender,kind,number,pcas,person,real]).
:-features(clg,[case,gender,number,person]).
:-features(cll,[case,gender,number,pcas,person]).
:-features(cln,[case,gender,number,person]).
:-features(clneg,[clneg]).
:-features(clr,[number,person]).
:-features(comp,[gender,number,person]).
:-features(csu,[que,wh]).
:-features(det,[def,dem,det,gender,number,numberposs,person,poss,wh]).
:-features(ht,[arg0,arg1,arg2,cat,diathesis,refl]).
:-features(incise,[incise_kind,position]).
:-features(nc,[gender,hum,number,person,time]).
:-features(np,[gender,hum,number,person]).
:-features(number,[gender]).
:-features(prel,[extracted,fct,gender,kind,number,pcas,person,real]).
:-features(prep,[pcas]).
:-features(pri,[fct,gender,number,person]).
:-features(pro,[def,dem,fct,gender,hum,number,numberposs,person]).
:-features(v,[aspect,aux_req,diathesis,gender,mode,number,person,tense]).
:-features(xarg,[gender,number]).


%%% Default Feature Structure attached to each category

:-tag_features('CS','CS'{},'CS'{}).
:-tag_features('Infl','Infl'{},'Infl'{}).
:-tag_features('N','N'{},'N'{}).
:-tag_features('N2','N2'{},'N2'{}).
:-tag_features('PP','PP'{},'PP'{}).
:-tag_features('S','S'{},'S'{}).
:-tag_features('V','V'{},'V'{}).
:-tag_features('V1','V1'{},'V1'{}).
:-tag_features('VArg','VArg'{},'VArg'{}).
:-tag_features(adj,adj{},adj{}).
:-tag_features(adjP,adjP{},adjP{}).
:-tag_features(advneg,advneg{},advneg{}).
:-tag_features(arg,arg{},arg{}).
:-tag_features(aux,aux{},aux{}).
:-tag_features(cla,cla{},cla{}).
:-tag_features(cld,cld{},cld{}).
:-tag_features(clg,clg{},clg{}).
:-tag_features(cll,cll{},cll{}).
:-tag_features(cln,cln{},cln{}).
:-tag_features(clneg,clneg{},clneg{}).
:-tag_features(clr,clr{},clr{}).
:-tag_features(comp,comp{},comp{}).
:-tag_features(csu,csu{},csu{}).
:-tag_features(det,det{},det{}).
:-tag_features(ht,ht{},ht{}).
:-tag_features(incise,incise{},incise{}).
:-tag_features(nc,nc{},nc{}).
:-tag_features(np,np{},np{}).
:-tag_features(number,number{},number{}).
:-tag_features(prel,prel{},prel{}).
:-tag_features(prep,prep{},prep{}).
:-tag_features(pri,pri{},pri{}).
:-tag_features(pro,pro{},pro{}).
:-tag_features(v,v{},v{}).
:-tag_features(xarg,xarg{},xarg{}).


%%% Definition of finite sets

%% :-finite_set('Vadj',[+,-]).
:-finite_set(aspect,[imperfect,perfect]).
:-finite_set('aux-req',[avoir,etre]).
:-finite_set(aux_req,[avoir,'�tre']).
:-finite_set(case,[-,acc,dat,gen,loc,nom,rfl]).
:-finite_set(cat,[-,'CS','N','N2','PP','S','V',adj,adv,advneg,aux,cla,cld,clg,cll,cln,comp,coo,csu,det,nc,np,number,poncts,prel,prep,pri,pro,v]).
%% :-finite_set(def,[+,-]).
:-finite_set(diathese,[active,passive]).
%% :-finite_set(diathesis,[active,passive]).
%% :-finite_set(excl,[ante,post]).
:-finite_set(extracted,[+,-]).
:-finite_set(extraction,[-,rel,wh]).
:-finite_set(flag,[+,-]).
:-finite_set(form,[comme,il,que,si,'�a']).
:-finite_set('form-aux',[aller,avoir,etre,venir,'venir-de']).
:-finite_set(gender,[-,fem,masc]).
%% :-finite_set(hum,[+]).
:-finite_set(incise_kind,[coma,dash,par]).
:-finite_set(inv,[+,-,cl]).
:-finite_set(kind,[-,acomp,ncomp,obj,prepacomp,prepobj,prepscomp,prepvcomp,scomp,subj,vcomp]).
:-finite_set(mode,[-,conditional,gerundive,imperative,indicative,infinitive,participle,subjonctive]).
%% :-finite_set(neg,[+,-]).
:-finite_set(number,[-,pl,sg]).
:-finite_set(person,[-,1,2,3]).
:-finite_set(prep,[+,-,a,apres,'apr�s',autre,avec,comme,contre,dans,de,devant,en,par,pour,sur,vers,'�']).
%% :-finite_set(que,[+,-]).
%% :-finite_set(refl,[+,-]).
:-finite_set(sat,[+,-,ppart]).
:-finite_set(tense,[-,future,'future-perfect',imperfect,past,'past-anterior','past-historic',perfect,pluperfect,present]).
%% :-finite_set(time,[+,-]).
:-finite_set(type,[card,def,dem,excl,ind,int,ord,part,poss,qual,rel]).
:-finite_set('v-form',[infinitive,participle,tense]).
%% :-finite_set(wh,[+,-]).


%%% Models of anchor constraints

