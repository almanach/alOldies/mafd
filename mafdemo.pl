#!/usr/bin/perl -w

use CGI::Pretty qw/-compile -nosticky  :standard *pre *table :html3 -private_tempfiles -oldstyle_urls/;
use CGI::Carp qw(fatalsToBrowser);
use strict;

# $CGI::POST_MAX=1024 * 500;  # max 500K posts

use POSIX qw(strftime);
use IPC::Open2;
use IPC::Run qw(start finish run pump);
use IO::Socket;
use File::Basename;

$SIG{CHLD} = 'IGNORE';


#$ENV{'PATH'} = '/bin:/usr/local/bin';
#$ENV{'DOTFONTPATH'} = '/usr/lib/openoffice/share/fonts/truetype';

local our $query = new CGI;
our $pattern = qr/(\w+)\s*(?:{(.*?)})?\((\d+),(\d+)\)/o;

our $dotcmd = '/usr/local/bin/dot';

our %accents = ( "e'" => ["e'",'�'],

             'a`' => ['a`','�'],
             'e`' => ['e`', '�'],
             'u`' => ['u`','�'],

             'a^' => ['a\^','�'],
             'e^' => ['e\^','�'],
             'i^' => ['i\^','�'],
             'o^' => ['o\^','�'],
             'u^' => ['u\^','�'],

             'e"' => ['e"','�'],
             'i"' => ['i"','�'],
             'u"' => ['u"','�'],
             'y"' => ['y"','�'],

             'c,' => ['c,(?=[aou])','�']

             );

our $accentpat = join('|',map( $accents{$_}->[0], keys(%accents)));
$accentpat = qr/($accentpat)/o;

our %graphics = ( 'gif' => { type => 'image/gif',
			     dot => [qw{-Gbgcolor=linen -Gcolor=palegreen -Gstyle=filled'}],
			     attachment => 'maf.gif' },
		  'png' => { type => 'image/png',
                            dot => [qw{-Gbgcolor=linen}],
                            attachment => 'maf.png' },
		  'ps' => {  type => 'application/postscript',
                            dot => [qw{-Gsize="8,8" -Gmargin=".5,4"}],
                            attachment => 'maf.ps'
			 },
		  'dot' => { type=> 'text/plain',
                            attachment => 'maf.dot',
			    dot => []
			  },
		  'xml' => { type=> 'text/xml',
                            attachment => 'maf.xml',
			    dot => []
			  },
                 'svgz' => { type=> 'image/svg+xml',
			     attachment => 'maf.svgz',
			     dot => []
			   }
	       );

######################################################################
# Configuration 

delete @ENV{'IFS', 'CDPATH', 'ENV', 'BASH_ENV'};

our $server = { 'host' => 'localhost',
	       'port' => '8997',
	   };


our %options = ();

######################################################################
# Reading params

our $path_info = $query->path_info;

our $file = $query->upload('filename');

if (!$file && $query->cgi_error) {
    print $query->header(-status=>$query->cgi_error);
    exit 0;
}

if ($file) {
    my $type = $query->uploadInfo($file)->{'Content-Type'};
    unless ($type eq 'text/plain') {
	die "PLAIN FILES ONLY!";
    }
}

our ($view) = $query->param(-name=>'view');

$view='none' unless defined $view;

our @history=();

our ($sentence) = &clean_sentence($query->param(-name=>'sentence'));

our ($format) = $query->param(-name=>'format');

our $turned_on = $query->param('save_history');

our $opt_compact = $query->param('opt_compact');
our $opt_standoff = $query->param('opt_standoff');
our $opt_reduced = $query->param('opt_reduced');
our $opt_embedded_tokens = $query->param('opt_embedded_tokens');
our $opt_tagset = $query->param('opt_tagset');
our $opt_hypertag = $query->param('opt_hypertag');

if (defined($file) && "$file") {
    @history=();
    while (<$file>){
	next if /^\d*\s*$/;
	$_ =  &clean_sentence($_);
	push(@history,$_) unless (/^$/);
    }
} elsif ("$turned_on" eq 'on') {
    @history = $query->cookie('maf');
} else {
    @history = $query->param('hidden');
}

if (defined $sentence && $sentence && ! grep("$_" eq $sentence,@history)) {
    @history=() if ($history[0] eq '<none>');
    @history=($sentence,@history);
#    $query->param(-name=>'history',-values=>[$sentence]);
}

if (!@history) {
    @history=('<none>');
}

$query->param(-name=>'history',-values=>[@history]);
$query->param('hidden',@history);

#my @options = $query->param('options');

######################################################################
# Emitting form

## $view = 'xml' if (defined $format && $graphics{$format}{type} eq 'text/xml');

if (defined $graphics{$view}) {
  print $query->header(-type=> $graphics{$view}{type},
	##	       -nph=>1,
		       -attachment=> $graphics{$view}{attachment},
                       -expires=>'+1h');
  goto RESULT;
}

our $cookie;

if (defined $turned_on && ("$turned_on" eq 'on')) {
    $cookie = $query->cookie(-name=>'maf',
			     -value=>\@history,
			     -expires=>'+1h',
			     );
    print $query->header(-cookie=>$cookie);
} else {
    print $query->header;
}

my $JSCRIPT=<<END;
// Put selected history line into sentence part (erasing old sentence)
    function paste_into_sentence(element) {
	var sentence = element.options[element.selectedIndex].text;
	document.form1.sentence.value = sentence;
	document.form1.sentence.focus();
	return true;
    }

var http_request = false;
function display_dag(sentence) {
      http_request = false;
      url = 'mafdemo.pl?sentence='+URLencode(sentence)+'&view=gif&Action=Submit';
      if (window.XMLHttpRequest) { // Mozilla, Safari,...
         http_request = new XMLHttpRequest();
         if (http_request.overrideMimeType) {
            http_request.overrideMimeType('text/xml');
         }
      } else if (window.ActiveXObject) { // IE
         try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
         } catch (e) {
            try {
               http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
         }
      }
      if (!http_request) {
         alert('Cannot create XMLHTTP instance');
         return false;
      }
      http_request.onreadystatechange = function () {
        if (http_request.readyState == 4) {
          if (http_request.status == 200) {
            obj = document.getElementById("DAG");
            parent = obj.parentNode;
            parent.removeChild(obj);
            imageNode = document.createElement('img');
            imageNode.setAttribute("src",url);
            imageNode.setAttribute("align","CENTER");
            parent.appendChild(imageNode);
          } else {
            alert('There was a problem with the request.');
          }
        }
      };
      http_request.open('GET', url, true);
      http_request.send(null);
   }

function URLencode(sStr) {
    return escape(sStr).
             replace(/\\+/g, '%2B').
                replace(/\\"/g,'%22').
                   replace(/\\'/g, '%27').
                     replace(/\\//g,'%2F');
  }

window.onload = function () { 
     dag = document.getElementById("DAG");
     if (dag) {
       sentence = dag.getAttribute("sentence");
       display_dag(sentence);
     }
};

END

my $STYLE=<<END;
<!--
    BODY {background-color: linen;}
    #waiting {
     text-decoration: blink; 
     color: red;
   }
-->
END

print $query->start_html( -title => 'MAF on line (for French)',
			  -script=> $JSCRIPT,
			  -style => { -code => $STYLE }
			  );
print h1('Using MAF on line (for French)');

print $query->a({href=> dirname(url(-path))."/mafhelp.html"},
		'Some documentation on Morphosyntactic Annotation Framework'),
  ' and ',
  $query->a({href=>  dirname(url(-path))."/maf.rnc"}, 'MAF Relax NG schema'),
  $query->a({href=>  dirname(url(-path))."/maf.dtd"}, 'MAF DTD');

print $query->start_multipart_form(-name=>'form1');

print p('Select options',
	$query->checkbox(-name=>'opt_compact',
			 -default=> 'off',
			 -label=>'compact FS'
			 ),
	$query->checkbox(-name=>'opt_standoff',
			 -default => 'off',
			 -label=>'standoff'
			 ),
	$query->checkbox(-name=>'opt_reduced',
			 -default =>  'off',
			 -label=>'reduced FSM'
			 ),
	$query->checkbox(-name=>'opt_embedded_tokens',
			 -default =>  'off',
			 -label=>'embedded Tokens'
			 ),
	$query->checkbox(-name=>'opt_tagset',
			 -default=> 'off',
			 -label=>'tagset'
			 ),
	$query->checkbox(-name=>'opt_hypertag',
			 -default =>  'off',
			 -label=>'syntax info'
			 ),
	);

print p('Enter a sentence (in French) or select one in the history popup menu',
	br,
	$query->textarea(-name=>'sentence', 
			 -rows=>3, -columns=>70)
       );

print p('History popup menu',
	br,
	$query->popup_menu(-name=>'history',
			   -onChange=>'paste_into_sentence(this)'
			  ),
	$query->hidden(-name=>'hidden',-default=>[@history]),
	$query->checkbox(-name=>'save_history',
			 -value=>'on',
			 -label=>'Save/Restore history (using a cookie)')
	);

print p('Load your own file as history popup menu (a sentence per line)',
	br,
	$query->filefield(-name=>'filename',
			  -size=>50
			  ));

print  $query->hidden(-name=>'format',-value=>'xml');

print  $query->hidden(-name=>'view',-default=>'none');

print p($query->reset,$query->submit('Action','Submit'));

print $query->endform;
print "<HR>\n";

######################################################################
# Emitting result

 RESULT:

goto FOOTER unless ($sentence);

if (!(defined $graphics{$view})) {
    # delegate the work to a sub query !
    my $url = $query->self_url;
    $url =~ s/(history|hidden)=(.*?);//g;
##    print "URL $url";
    $url =~ s/opt_(compact|reduced|tagset|embedded_tokens|standoff|syntax)=(.*?);//g;
    $url =~ s/(sentence=)/opt_standoff=on;$1/;
    my $psurl = $url;
    $psurl =~ s%view=\w+%view=ps%;
    my $pngurl = $url;
    $pngurl =~ s%view=\w+%view=png%;
    my $doturl = $url;
    $doturl =~ s%view=\w+%view=dot%;
    my $svgurl = $url;
    $svgurl =~ s%view=\w+%view=svgz%;
    my $xmlurl = $url;
    $xmlurl =~ s%view=\w+%view=xml%;
    print "[",$query->a({href=>$psurl},"PostScript"),
      "|",$query->a({href=>$pngurl},"PNG"),
        "|",$query->a({href=>$doturl},"Dot"),
          "|",$query->a({href=>$svgurl},"SVG"),
	    "|",$query->a({href=>$xmlurl},"XML"),"]\n";
    $url =~ s%view=\w+%view=gif%;
    print $query->div({align=>"CENTER"},
		      $query->p({id=>"DAG",sentence=>"$sentence"},
				span({id=>"waiting"},"waiting for image...")));
##    goto NOTHING;
}


#print "path_info=$path_info";

$sentence =~ s/$accentpat/$accents{$1}->[1]/og;

open(LOG,">> /tmp/cgi_maf.log") || 
    die "Can't open log file";

print LOG strftime("date= %b %e %Y %H:%M:%S\n", localtime);
print LOG "host=",remote_host(),"\n";
print LOG "sentence=",$sentence,"\n";
print LOG "\n";

close(LOG);

our @norm_options = ();
push(@norm_options,'--compact') if $opt_compact;
push(@norm_options,'--standoff') if $opt_standoff;
push(@norm_options,'--reduced') if $opt_reduced;
push(@norm_options,'--embedded_tokens') if $opt_embedded_tokens;
push(@norm_options,'--tagset') if $opt_tagset;
push(@norm_options,'--hypertag') if $opt_hypertag;
our $norm_options = join(' ',@norm_options);

print STDERR "OPTIONS: $norm_options $sentence\n";

my $handle = send_server($server,<<MSG);
options $norm_options
sentence $sentence
quit
MSG

## print "INFO $format $view\n";

##use XML::LibXSLT;
##use XML::LibXML;

if (defined $format && $format eq 'xml' && !defined $graphics{$view}) {

  my $convert = [qw{/usr/bin/xsltproc /var/www/perl/maf/maf.xsl -}];

  my $h = start $convert,
    '<',$handle,
      '>pipe',\*TATA,
	'2>pipe',\*ERR
	  or die "returned $?" ;
  print <TATA>;
  close TATA || die "can't close: $!";
  finish $h;
} elsif ($view eq 'xml') {
  print <$handle>;
  close($handle);
  goto NOTHING;
} elsif ($view eq 'dot') {
  my $convert = [qw{/usr/bin/xsltproc /var/www/perl/maf/maf2dag.xsl -}];
  my $cmd = [$dotcmd,"-T$view",@{$graphics{$view}{dot}}];
  my $h = start $convert,
    '<',$handle,
      '>pipe',\*TATA,
	'2>pipe',\*ERR
	  or die "@$cmd returned $?" ;
  print <TATA>;
  close TATA || die "can't close: $!";
  finish $h;
  goto NOTHING;
} elsif (defined $graphics{$view}) { # dot format
  my $convert = [qw{/usr/bin/xsltproc /var/www/perl/maf/maf2dag.xsl -}];
  my $cmd = [$dotcmd,"-T$view",@{$graphics{$view}{dot}}];
  my $h = start $convert,
    '<',$handle,
      '|', $cmd,,
	'>pipe',\*TATA,
	  '2>pipe',\*ERR
	    or die "@$cmd returned $?" ;
   print <TATA>;
   close TATA || die "can't close: $!";
   finish $h;
   goto NOTHING;
} else {
  print <$handle>;
  close $handle || die "cannot close: $!";
}

goto NOTHING if (defined $graphics{$view});

#print $query->pre("FOREST FORMAT $format $formats{$format}");
#print "Send $sentence to $component\n";

#while (my $line = <$handle>) {
#  print $line;
#  if ($line =~ /^<\?xml/) {
#    print <<HEADER;
#<?xml-stylesheet type="text/xsl" href="maf.xsl"?>
#HEADER
#  }
#}

##exit;

FOOTER:
    
print hr,
    address(a({href=>'mailto:Eric.Clergerie@inria.fr'},'Eric de la Clergerie')),
    a({href=>'http://atoll.inria.fr/~clerger/'},'Home Page');

print $query->end_html;

NOTHING:

exit;

######################################################################
# sub routines

sub clean_sentence {
    my ($sentence)=@_;
    $sentence ||= "";
    $sentence =~ s/\s+/ /og;
    $sentence =~ s/^\s+//og;
    $sentence =~ s/\s+$//og;
    return $sentence;
}

sub send_server {
    my $server = shift;
    my $message = shift;

    my $host = $server->{'host'};
    my $port = $server->{'port'};

    my ($kidpid, $handle, $line);

    # create a tcp connection to the specified host and port
    $handle = IO::Socket::INET->new(Proto     => "tcp",
				    PeerAddr  => $host,
				    PeerPort  => $port
				)
	or die "can't connect to the maf server [$host:$port]";

    $handle->autoflush(1);

    # split the program into two processes, identical twins
    die "can't fork: $!" unless defined($kidpid = fork());

    # Child part
    if (!$kidpid) { # Child
      # print $handle $message;
      #      $r->cleanup_for_exec(); # untie the socket
      close(STDIN);
      close(STDOUT);
      close(STDERR);
      print $handle $message;
      CORE::exit 0;
    }
    
    return $handle;
}
